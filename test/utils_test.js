'use strict';

const expect = require("chai").expect;

const chai = require("chai");
const chaiAsPromised = require("chai-as-promised");

chai.use(chaiAsPromised);

const utils = require("../lib/utils");

const movieResource = "https://content.viaplay.se/pc-se/film/the-danish-girl-2015";
const errorMovieResource = "https://content.viaplay.se/pc-se/film/the-danish-girl-20argtstr15";
const imdbId = "tt0810819";
const theMovieDBId = 306819;
const youtubeId = "d88APYIGkjk";

describe("IMDBIdPromise", function() {

  it("should get the correct IMDB id", function() {
    return expect(utils.IMDBIdPromise(movieResource)).eventually.equal(imdbId);
  });
  it("should not get an IMDB id", function() {
    return expect(utils.IMDBIdPromise(errorMovieResource)).eventually.be.rejectedWith("Not found movie resource");
  });
});

describe("theMovieDBIdPromise", function() {

  it("should get the correct movie ID", function() {
    return expect(utils.theMovieDBIdPromise(imdbId)).eventually.equal(theMovieDBId);
  });
});

describe("theMovieDBIdPromise", function() {

  it("should get the correct movie ID", function() {
    return expect(utils.theMovieDBIdPromise(imdbId)).eventually.equal(theMovieDBId);
  });
});

describe("the YouTube Id Promise", function() {

  it("should got correct id", function() {
    return expect(utils.youTubeIdPromise(theMovieDBId)).eventually.equal(youtubeId);
  });
});

describe("error message helper", function() {
  let errorMessage = utils.getErrorMessageJSON("An error occurred", 500);

  it("should return a json with error message", function() {
    return expect(errorMessage).to.have.property("Error", "An error occurred");
  });
  it("should return a json with status code", function() {
    return expect(errorMessage).to.have.property("Status", 500);
  });
});
