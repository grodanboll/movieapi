'use strict';

const expect = require("chai").expect;
const request = require('supertest');
const chai = require("chai");
const chaiAsPromised = require("chai-as-promised");

const constants = require("./../lib/constants");

chai.use(chaiAsPromised);

const host = constants.config.apiHost;

describe("Routes", function() {

  it("should get youtube link if passing a movie resource with trailer as query string", function(done) {
    request(host)
      .get("/api/movie?link=https://content.viaplay.se/pc-se/film/the-danish-girl-2015")
      .end(function(err, res) {
        if (err) {
          throw err;
        }
        expect(res.body.trailer).to.equal("https://youtube.com/watch?v=d88APYIGkjk");
        done();
      });
  });

  it("should get not found `Not found movie resource` if passing a non existent movie resource", function(done) {
    request(host)
      .get("/api/movie?link=https://content.viaplay.se/pc-se/film/the-english-girl-2015")
      .end(function(err, res) {
        expect(res.body.Error).to.equal("Not found movie resource");
        done();
      });
  });

  it("should get status not found trailer link if passing a movie resource without trailer", function(done) {
    request(host)
      .get("/api/movie?link=https://content.viaplay.se/pc-se/film/the-crossing-2012")
      .end(function(err, res) {
        expect(res.body.Error).to.equal("No trailer found");
        done();
      });
  });

  it("should get `No movie link found` if not passing a movie resource", function(done) {
    request(host)
      .get("/api/movie")
      .end(function(err, res) {
        expect(res.body.Error).to.equal("No movie link found");
        done();
      });
  });
});
