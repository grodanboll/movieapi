'use strict';
const fetch = require('node-fetch');
const constants = require('./constants');

function getIMDBId(json) {
  return json._embedded["viaplay:blocks"][0]._embedded["viaplay:product"].content.imdb.id;
}

function getTheMovieDbId(json) {
  if (!json.movie_results.length > 0) {
    return Promise.reject(new Error("No movie found in TMDb"));
  }
  return json.movie_results[0].id;
}
function getYouTubeId(json) {
  if (!json.results.length > 0) {
    return Promise.reject(new Error("No trailer found"));
  }
  if (json.results[0].site !== "YouTube") {
    return Promise.reject(new Error("No YouTube trailer found"));
  }
  return json.results[0].key;
}

function parseJson(response) {
  return response.json();
}

function status(response) {
  if (response.status >= 200 && response.status < 300) {
    return Promise.resolve(response)
  } else if (response.status === 404) {
    return Promise.reject(new Error("Not found movie resource"));
  }
  return Promise.reject(new Error(response.statusText));
}

exports.getErrorMessageJSON = function getErrorMessageJSON(errorMessage, statusCode) {
  return {
    "Error" : errorMessage,
    "Status" : statusCode
  };
};

exports.IMDBIdPromise = function IMDBIdPromise(movieResource) {
  return new Promise((resolve, reject) => {
    fetch(movieResource)
      .then(status)
      .then((result) => { return result; },
        (err) => { reject(err.message, err); })
      .then(parseJson)
      .then(getIMDBId, (err) => { reject(err.message, err); })
      .then((id) => { resolve(id); },
        (err) => { reject("Not found IMDB info in movie resource", err); })
      .catch((err) => {
        reject("Error in fetch movie resource", err);
      });
  });
};

exports.theMovieDBIdPromise = function theMovieDBIdPromise(imdbID) {
  return new Promise((resolve, reject) => {
    let endpoint = constants.config.theMovieDBApiUrl + "/find/" + imdbID + "?external_source=imdb_id&api_key=" + constants.config.theMovieDBApiKey;
    fetch(endpoint)
      .then(status)
      .then((result) => { return result; },
        (err) => { reject(err.message, err); })
      .then(parseJson)
      .then(getTheMovieDbId,(err) => { reject(err.message, err); })
      .then((id) => { resolve(id); },
        (err) => { reject("Not found the movie database id", err); })
      .catch((err) => {
        reject("Error in get The movie DB ID:", err);
      });
  });
};

exports.youTubeIdPromise = function youTubeIdPromise(theMovieDBId) {
  return new Promise((resolve, reject) => {
    let endpoint = constants.config.theMovieDBApiUrl + "/movie/" + theMovieDBId+ "/videos?external_source=imdb_id&api_key=" + constants.config.theMovieDBApiKey;
    fetch(endpoint)
      .then(status)
      .then((result) => { return result; },
        (err) => { reject(err.message, err); })
      .then(parseJson)
      .then(getYouTubeId)
      .then(
        (youTubeID) => { resolve(youTubeID); },
        (err) => {
          reject(err.message, err);
        })
      .catch((err) => {
        reject(err);
      });
  });
};
