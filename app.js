const express = require("express");
const morgan = require("morgan");
const utils = require("./lib/utils");
const constants = require("./lib/constants");
const app = express();

let port = process.env.PORT || 3000;

app.use(morgan("dev"));

app.use("/api/movie", function(req, res){
  if (!req.query.link) {
    res.status(404);
    res.send(utils.getErrorMessageJSON("No movie link found", 404));
  }
  let movieResource = req.query.link;
  utils.IMDBIdPromise(movieResource).then((imdbID) => {
    utils.theMovieDBIdPromise(imdbID).then((movieId) => {
      utils.youTubeIdPromise(movieId).then((id) => {
        res.send({
          "trailer": constants.config.youtubeBaseUrl + id
        });
      }, (err) => {
        res.status(404);
        res.send(utils.getErrorMessageJSON(err, 404));
      }).catch((err) => {
        res.status(500);
        res.send(utils.getErrorMessageJSON(err, 500));
      });
    }).catch((err) => {
      res.status(500);
      res.send(utils.getErrorMessageJSON(err, 500));
    });
  }, (err) => {
    res.status(404);
    res.send(utils.getErrorMessageJSON(err, 404));

  }).catch((err) => {
    res.status(500);
    res.send(utils.getErrorMessageJSON(err, 500));
  });
});

app.get("/", function(req,res){
  res.send("Idas movie API");
});

app.listen(port, function() {
  console.log("Running on port:", port);
});
