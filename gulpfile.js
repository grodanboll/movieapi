let gulp = require('gulp');
let gulpmon = require('gulp-nodemon');

gulp.task('default', function() {
  gulpmon({
    script: 'app.js',
    ext: 'js',
    env: {
      PORT: 8000
    },
    ignore: ['./node_modules/**']
  })
  .on('restart', function(){
    console.log('Restarting');
  });
});
