# README #

This is a simple API that takes a movie recource and respond with a link to a trailer on YouTube.
The movie resource should be sent as a querystring  eg.

```
/api/movie?link=https://content.viaplay.se/pc-se/film/ted-2-2015
```

If it not finds a trailer, it responds with status 404 Not found.
If it finds a trailer on YouTube it responds with the YouTube link eg.

```JSON
{
    "trailer": "https://youtube.com/watch?v=S3AVcCggRnU"
}
```
The API is built with Express.js framework.

### How to set it up? ###
Get a free api key for The Movie Database API
Add the key in lib/constants.js

Clone this repo and run:

```javascript
npm install
```
then

```javascript
gulp
```

The server will run on local host port 8000.

### How to run tests ###
```javascript
npm test
```
the test uses mocha and chai.
for promises it uses chai-as-promised

the tests for routs are a bit unstable.
they sometimes fail and sometimes success.

### External resources ###
[Viaplay Content API](https://content.viaplay.se/pc-se/film)

[The movie Database API](https://www.themoviedb.org/documentaiton/api)

### Who do I talk to? ###

* Ida Brogie (ida.brogie@gmail.com)
